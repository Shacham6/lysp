from lysp.environment import Environment
from lysp.backend.types.ly_object import LyObject


class LyFunction(LyObject):
    def __init__(self, interpreter, parameter_names, body, defining_env):
        self.__interpreter = interpreter
        self.__parameter_names = [param_name.token.lexeme for param_name in parameter_names]
        self.__body = body
        self.__defining_env = defining_env

    def __call__(self, *args):
        evaluated_params = [self.__interpreter.eval(arg)
                            for arg in args]
        local_environment = Environment(dict(zip(self.__parameter_names, evaluated_params)), self.__defining_env)
        return self.__interpreter.execute_block(self.__body, local_environment)
