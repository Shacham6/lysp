from lysp.environment import Environment
from lysp.parser import Grouping, List
from .ly_object import LyObject


class LyMacro(LyObject):
    def __init__(self, interpreter, parameter_names, body, defining_env):
        self.__interpreter = interpreter
        self.__parameter_names = [parname.token.lexeme for parname in parameter_names]
        self.__body = body
        self.__defining_env = defining_env

    def __call__(self, *args):
        local_environment = TransitiveEnvironment(dict(zip(self.__parameter_names, args)), self.__defining_env)
        expanded_body = expand_body(self.__body, self.__interpreter, local_environment)
        return self.__interpreter.execute_block(expanded_body, local_environment)


class TransitiveEnvironment(Environment):
    def __init__(self, items, outer):
        Environment.__init__(self, items, outer)
        self.__outer = outer

    def set(self, name, value):
        return self.__outer.set(name, value)


def expand_body(body, interpreter, env):
    if isinstance(body, (Grouping, List)):
        if isinstance(body, Grouping) and body.head.token.lexeme == 'unquote':
            return interpreter.execute_block([body], env)

        items = []
        for item in body.items:
            result = expand_body(item, interpreter, env)
            items.append(result)
        return type(body)(*items)
    elif isinstance(body, list):
        return [expand_body(item, interpreter, env) for item in body]
    return body
