from attr import attrs, attrib
from lysp.backend.types import LyObject
import stringcase


class LyExpr:
    def accept(self, visitor):
        class_name = self.__class__.__name__
        visit_method_name = f"visit_{stringcase.snakecase(class_name)}"
        return getattr(visitor, visit_method_name)(self)


@attrs(repr=False)
class StringLiteral(LyExpr, LyObject):
    token = attrib()


@attrs
class NumberLiteral(LyExpr, LyObject):
    token = attrib()


@attrs
class Symbol(LyExpr, LyObject):
    token = attrib()


@attrs
class Keyword(LyExpr, LyObject):
    token = attrib()


@attrs(init=False)
class Grouping(LyExpr, LyObject):
    items = attrib()

    def __init__(self, head, *rest):
        self.items = [head, *rest]

    @property
    def head(self):
        return self.items[0]

    @property
    def rest(self):
        return self.items[1:]


@attrs(str=False)
class List(LyExpr, LyObject):
    items = attrib()

    def __str__(self):
        return str(self.items)
