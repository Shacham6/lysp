from .ly_object import LyObject


class LyNativeFunction(LyObject):
    Symbol = "LyNativeFunction"
