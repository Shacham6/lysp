def require(item, requirement):
    if isinstance(requirement, type):
        require_item_is_of_type(item, requirement)


def require_item_is_of_type(item, type_):
    if not isinstance(item, type_):
        raise SyntaxError(f"Required type {type_}, instead got type {item.__class__.__name__}")
