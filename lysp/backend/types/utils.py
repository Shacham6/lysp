def expand_symbols(functions):
    mappings = {}
    for func in functions:
        mappings[func.Symbol] = func
    return mappings
