class LyObject:

    @classmethod
    def type_name(cls):
        return cls.__name__

    def __str__(self):
        return f"<{self.type_name()} => {id(self)}>"
