from lysp.backend.types import LyNativeFunction, utils
from functools import reduce
import operator as op


class __Op(LyNativeFunction):
    def __init__(self, name, symbol, operator):
        self.Symbol = symbol
        self.__name = name
        self.__operator = operator

    def type_name(self):
        return self.__name

    def __call__(self, head, target, *rest):
        return reduce(self.__operator, [head, target, *rest])


export = utils.expand_symbols([
    __Op('Plus', '+', op.add),
    __Op('Minus', '-', op.sub),
    __Op('Mul', '*', op.mul),
    __Op('Div', '/', op.truediv),
    __Op('Eq', '=', op.eq),
    __Op('Gt', '>', op.gt),
    __Op('Lt', '<', op.lt),
    __Op('Ge', '>=', op.ge),
    __Op('Le', '<=', op.le),
    __Op('And', 'and', op.and_),
    __Op('Or', 'or', op.or_),
])
pass
