from lysp.backend.types import LyNativeFunction, utils


class Print(LyNativeFunction):
    Symbol = "print"

    def __call__(self, *args, **kwargs):
        return print(*args, **kwargs)


export = utils.expand_symbols([Print()])
