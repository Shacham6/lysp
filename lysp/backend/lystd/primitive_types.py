from lysp.backend.types.ly_object import LyObject
from lysp.backend.types.validation import require


class LyString(LyObject):
    def __init__(self, value):
        self.__value = value

    def __str__(self):
        return self.__value

    def __add__(self, other):
        return LyString(self.__value + str(other))


class LyFloat(LyObject):
    def __init__(self, value):
        self.__value = value

    def __str__(self):
        return str(self.__value)

    def __add__(self, other):
        require(other, LyFloat)
        return LyFloat(self.__value + other.__value)

    def __sub__(self, other):
        require(other, LyFloat)
        return LyFloat(self.__value - other.__value)

    def __mul__(self, other):
        require(other, LyFloat)
        return LyFloat(self.__value * other.__value)

    def __truediv__(self, other):
        require(other, LyFloat)
        return LyFloat(self.__value / other.__value)

    def __eq__(self, other):
        require(other, LyFloat)
        return LyBool(self.__value == other.__value)

    def __le__(self, other):
        require(other, LyFloat)
        return LyBool(self.__value <= other.__value)


class LyBool(LyObject):
    def __init__(self, value):
        self.__value = value

    def __bool__(self):
        return bool(self.__value)
