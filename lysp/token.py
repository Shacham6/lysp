import attr


@attr.s
class Token:
    name = attr.ib()
    lexeme = attr.ib()
    line = attr.ib()
    column = attr.ib()


SKIP = "SKIP"
NEWLINE = "NEWLINE"
COMMENT = "COMMENT"
LEFT_PAREN = "LeftParen"
RIGHT_PAREN = "RightParen"
LEFT_BRACKET = "LeftBracket"
RIGHT_BRACKET = "RightBracket"
NUMBER_LITERAL = "NumberLiteral"
STRING_LITERAL = "StringLiteral"
KEYWORD = "Keyword"
IDENTIFIER = "Identifier"
