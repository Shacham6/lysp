class Environment:
    def __init__(self, items, outer=None):
        self.__items = items
        self.__outer = outer

    def find(self, name):
        if name in self.__items:
            return self.__items[name]
        if self.__outer is not None and name in self.__outer:
            return self.__outer.find(name)
        raise EnvironmentError(f"Undefined global reference: {name}")

    def set(self, name, value):
        self.__items[name] = value

    def __contains__(self, name):
        return name in self.__items
