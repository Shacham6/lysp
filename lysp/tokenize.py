import re

from lysp.token import (
    Token, SKIP, NEWLINE, COMMENT, LEFT_PAREN,
    RIGHT_PAREN, LEFT_BRACKET, RIGHT_BRACKET, NUMBER_LITERAL,
    STRING_LITERAL, KEYWORD, IDENTIFIER,
)


def named_group_regex(name, regex):
    return fr"(?P<{name}>{regex})"


def compile_group(patterns):
    return re.compile("|".join(named_group_regex(name, regex) for name, regex in patterns))


__RULES = compile_group([
    (SKIP, r"[ \t]+"),
    (NEWLINE, r"\n"),
    (COMMENT, r"#[^\n]+"),
    (LEFT_PAREN, r"\("),
    (RIGHT_PAREN, r"\)"),
    (LEFT_BRACKET, r"\["),
    (RIGHT_BRACKET, r"\]"),
    (NUMBER_LITERAL, r"-?[0123456789]+(\.[1234567890]+)?"),
    (STRING_LITERAL, r'"[^"]*"'),
    (KEYWORD, r":[^ \t\n()\[\]]+"),
    (IDENTIFIER, r"[^ \t\n()\[\]]+"),
])


def tokenize(source_code):
    for match in __RULES.finditer(source_code):
        if match.lastgroup in ("SKIP", "NEWLINE", "COMMENT"):
            continue

        yield Token(match.lastgroup, match.group(), 1, match.lastindex)
