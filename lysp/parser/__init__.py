from lysp.backend.types.metatypes import (
    StringLiteral, NumberLiteral,
    Symbol, Grouping, List, Keyword
)


def parse(tokens):
    return Parser(list(tokens)).wide_parse()


class Parser:
    def __init__(self, tokens):
        self.__tokens = tokens

    def wide_parse(self):
        if len(self.__tokens) == 0:
            return []

        items = []

        while len(self.__tokens) > 0:
            items.append(self.parse())
        return items

    def is_start_of_grouping(self):
        return self.__tokens[0].name == 'LeftParen'

    def is_start_of_list(self):
        return self.__tokens[0].name == 'LeftBracket'

    def is_singular(self):
        return self.__tokens[0].name in ('StringLiteral', 'NumberLiteral', 'Identifier')

    def parse(self):
        if self.is_start_of_grouping():
            return self.grouping()
        elif self.is_start_of_list():
            return self.list()
        elif self.is_singular():
            return self.singular()
        raise SyntaxError(f"What is {self.__tokens[0].name}?")

    def grouping(self):
        self.__tokens.pop(0)  # Drop first LeftParen
        items = []
        while not self.__tokens[0].name == 'RightParen':
            items.append(self.parse())
        self.__tokens.pop(0)
        return Grouping(*items)

    def list(self):
        self.__tokens.pop(0)  # Drop first LeftParen
        items = []
        while not self.__tokens[0].name == 'RightBracket':
            items.append(self.parse())
        self.__tokens.pop(0)
        return List(items)

    def singular(self):
        name = self.__tokens[0].name
        if name == 'StringLiteral':
            return StringLiteral(self.__tokens.pop(0))
        elif name == 'NumberLiteral':
            return NumberLiteral(self.__tokens.pop(0))
        elif name == 'Identifier':
            return Symbol(self.__tokens.pop(0))
        elif name == 'Keyword':
            return Keyword(self.__tokens.pop(0))
        raise SyntaxError(f"What is {name}?")
