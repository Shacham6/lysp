class Printer:
    def visit_string_literal(self, string_literal):
        return string_literal.token.lexeme

    def visit_number_literal(self, number_literal):
        return number_literal.token.lexeme

    def visit_identifier(self, identifier):
        return identifier.token.lexeme

    def visit_grouping(self, grouping):
        if len(grouping.params) == 0:
            return f"({ast_print(grouping.callable_)})"

        return f"({ast_print(grouping.callable_)} {ast_print(grouping.params)})"


__printer = Printer()


def ast_print(items):
    if not hasattr(items, '__iter__'):
        return items.accept(__printer)

    return " ".join([item.accept(__printer) for item in items])
