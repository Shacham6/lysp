from lysp.backend.types import LyMacro
from lysp.backend.types.ly_function import LyFunction
from lysp.backend.lystd.primitive_types import LyString, LyFloat
from more_itertools import last


class Interpreter:
    def __init__(self, env):
        self.__env = env

    def visit_string_literal(self, string_literal):
        return LyString(string_literal.token.lexeme[1:-1])

    def visit_number_literal(self, number_literal):
        return LyFloat(float(number_literal.token.lexeme))

    def visit_symbol(self, identifier):
        return self.__env.find(identifier.token.lexeme)

    def visit_grouping(self, grouping):
        head_lexeme = grouping.head.token.lexeme
        if head_lexeme == 'set':
            return self.set_decl(grouping)
        elif head_lexeme == 'do':
            return self.do_clause(grouping)
        elif head_lexeme == 'if':
            return self.if_clause(grouping)
        elif head_lexeme == 'fn':
            return self.fn_clause(grouping)
        elif head_lexeme == 'macro':
            return self.macro_clause(grouping)
        elif head_lexeme == 'quote':
            return self.quote_clause(grouping)
        elif head_lexeme == 'unquote':
            return self.unquote_clause(grouping)
        else:
            return self.invoke_callable(grouping)
            # params = [self.eval(param) for param in grouping.rest]
            # return self.eval(grouping.head)(*params)

    def set_decl(self, grouping):
        value = self.eval(grouping.rest[1])
        self.__env.set(grouping.rest[0].token.lexeme, value)

    def do_clause(self, grouping):
        latest_result = None
        for action in grouping.rest:
            latest_result = self.eval(action)
        return latest_result

    def if_clause(self, grouping):
        if self.eval(grouping.rest[0]):
            return self.eval(grouping.rest[1])
        return self.eval(grouping.rest[2])

    def fn_clause(self, grouping):
        params = grouping.rest[0].items
        body = []
        if len(grouping.rest) > 1:
            body = grouping.rest[1:]

        return LyFunction(self, params, body, self.__env)

    def macro_clause(self, grouping):
        params = grouping.rest[0].items
        return LyMacro(self, params, grouping.rest[1:], self.__env)

    def quote_clause(self, grouping):
        return grouping.rest[0]

    def unquote_clause(self, grouping):
        result = self.eval(grouping.rest[0])
        return result

    def invoke_callable(self, grouping):
        callable_ = self.__env.find(grouping.head.token.lexeme)
        if isinstance(callable_, LyMacro):
            return callable_(*grouping.rest)
        evaluated_params = (self.eval(param) for param in grouping.rest)
        return callable_(*evaluated_params)

    def visit_list(self, list_):
        return [self.eval(item) for item in list_]

    def visit_keyword(self, keyword):
        return keyword.token.lexeme

    def eval(self, item):
        return item.accept(self)

    def execute_block(self, body, env):
        previous = self.__env
        try:
            self.__env = env
            results = [self.eval(body_part) for body_part in body]
            return last(results, None)
        finally:
            self.__env = previous
