from lysp.backend.lystd import export_std
from lysp.interpreter.interpreter import Interpreter
from lysp.environment import Environment


def lysp_eval(item, env=None):
    if env is None:
        env = __env
    return Interpreter(env).eval(item)


__env = Environment(export_std)
